<?php

namespace App\Models;
use CodeIgniter\Model;

class FPDualModel extends Model{
    protected $table      = 'fpdual';
    protected $primaryKey = 'ID_FPD';
    protected $allowedFields = ['ID_FPD','IMPORTE', 'H_TOTALES', 'H_EMPRESA', 'H_CENTRO', 'DNI_ALU', 'FECHA_INI', 'FECHA_FIN', 'nºconvenio'];

    public function getFPDual($where){
    return $this->where($where)->first();
    }
}
