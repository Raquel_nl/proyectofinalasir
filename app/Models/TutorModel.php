<?php

namespace App\Models;
use CodeIgniter\Model;

class TutorModel extends Model{
    protected $table      = 'tutor';
    protected $primaryKey = ['CIF_EMP', 'DNI_INS'];
    protected $allowedFields = ['CIF_EMP', 'DNI_INS'];

    public function getTutor($where){
    return $this->where($where)->first();
    }
}
