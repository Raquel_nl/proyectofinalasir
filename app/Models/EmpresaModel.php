<?php

namespace App\Models;
use CodeIgniter\Model;

class EmpresaModel extends Model{
    protected $table      = 'empresa';
    protected $primaryKey = 'CIF';
    protected $allowedFields = ['CIF', 'NOMBRE', 'PROVINCIA', 'PAIS', 'DIRECCION', 'POBLACION', 'TLF', 'CP', 'EMAIL', 'PDEMAIL', 'REPRESENTANTE', 'NIF_REPRESENTANTE'];
    protected $returnType = 'object';
}


	 	