<?php

namespace App\Models;
use CodeIgniter\Model;

class InstructorModel extends Model{
    protected $table      = 'instructor';
    protected $primaryKey = 'DNI_INS';
    protected $allowedFields = ['DNI_INS', 'EMAIL', 'NOMBRE', 'TLF', 'ID_FPD'];

    public function getInstructor($where){
    return $this->where($where)->first();
    }
}
