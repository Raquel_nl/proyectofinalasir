<?php

namespace App\Models;
use CodeIgniter\Model;

class CentroTrabajoModel extends Model{
    protected $table      = 'c_trabajo';
    protected $primaryKey = 'ID_CT';
    protected $allowedFields = ['ID_CT','NOMBRE', 'PROVINCIA', 'DIRECCION', 'TLF', 'CIF_EMP'];
    protected $validationRules = [
        'ID_CT'     => 'required|numeric',
       
    ];

    public function getCentroTrabajo($where){
    return $this->where($where)->first();
    }
}
