<?php

namespace App\Models;
use CodeIgniter\Model;

class AlumnosModel extends Model{
    protected $table      = 'alumnos';
    protected $primaryKey = 'DNI_ALU';
    protected $allowedFields = ['DNI_ALU', 'NOMBRE', 'EMAIL', 'APELLIDO1', 'APELLIDO2', 'TLF'];

    public function getAlumno($where){
    return $this->where($where)->first();
    }
}



