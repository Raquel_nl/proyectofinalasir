<?php

namespace App\Models;
use CodeIgniter\Model;

class ConvenioModel extends Model{
    protected $table      = 'convenio';
    protected $primaryKey = 'nºconvenio';
    protected $allowedFields = ['nºconvenio', 'caducidad', 'representante', 'cif_emp', 'empresa', 'fecha'];

    public function getConvenio($where){
    return $this->where($where)->first();
    }
}
