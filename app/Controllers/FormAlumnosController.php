<?php

namespace App\Controllers;
use App\Models\AlumnosModel;

class FormAlumnosController extends BaseController{
    
    public function index(){
        helper('form');
        $data = new FormAlumnosController();
        echo view('formularios/alumnosform');
    }
    
    public function insertaralumno() {
        helper('form');
        $data = new AlumnosModel();
        $alumno = [
            "DNI_ALU" => $this->request->getPost('DNI_ALU'),
            "NOMBRE" => $this->request->getPost('NOMBRE'),
            "APELLIDO1" => $this->request->getPost('APELLIDO1'),
            "APELLIDO2" => $this->request->getPost('APELLIDO2'),
            "EMAIL" => $this->request->getPost('EMAIL'),
            "TLF" => $this->request->getPost('TLF'),
            
        ];

        $data->insert($alumno);
        //print_r($alumno);
        echo " ";
        return redirect()->to(site_url('TablaAlumnosController'));
        
    }}
