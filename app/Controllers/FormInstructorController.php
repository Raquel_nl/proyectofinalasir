<?php

namespace App\Controllers;
use App\Models\InstructorModel;

class FormInstructorController extends BaseController{
    
    public function index(){
        helper('form');
        $data = new FormInstructorController();
        echo view('formularios/instructorform');
    }
    
    public function insertarinstructor() {
        helper('form');
        $data = new InstructorModel();
        $instructor = [
            "DNI_INS" => $this->request->getPost('DNI_INS'),
            "EMAIL" => $this->request->getPost('EMAIL'),
            "NOMBRE" => $this->request->getPost('NOMBRE'),
            "TLF" => $this->request->getPost('TLF'),
            "ID_FPD" => $this->request->getPost('ID_FPD'),
                        
        ];

        $data->insert($instructor);
        //print_r($instructor);
        echo " ";
        return redirect()->to(site_url('TablaInstructorController'));
}}