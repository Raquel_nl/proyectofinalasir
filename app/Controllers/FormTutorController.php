<?php

namespace App\Controllers;
use App\Models\TutorModel;

class FormTutorController extends BaseController{
    
    public function index(){
        helper('form');
        $data = new FormTutorController();
        echo view('formularios/tutorform');
    }
    
    public function insertartutor() {
        helper('form');
        $data = new TutorModel();
        $tutor = [
            "CIF_EMP" => $this->request->getPost('CIF_EMP'),
            "DNI_INS" => $this->request->getPost('DNI_INS'),
            
        ];

        $data->insert($tutor);
        //print_r($tutor);
        echo " ";
        return redirect()->to(site_url('TablaTutorController'));
}}