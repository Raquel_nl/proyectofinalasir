<?php

    //Ésta es la carpeta dónde se almacena el nuevo Controlador.
    namespace App\Controllers;

    //Aqui lo seleccionamos para enlazarlo con el Modelo.
    use App\Models\EmpresaModel;
    use App\Models\CentroTrabajoModel;

    /* Después añadimos la clase con el nuevo controlador
    y lo enlazamos con la base de datos 'centro' y a su vez la tabla 'alumnos'.
    También se tiene que enlazar a su vez con el Modelo y la Vista que hemos creado. */
    class TablaCTController extends BaseController {

        public function index() 
        {
           $centrotrabajos = new CentroTrabajoModel();
           $datos ['centrotrabajos'] = $centrotrabajos->findAll();
           echo view('tablas/centrotrabajotabla', $datos);
        }
 
        public function busca_empresa() {
            helper(['form']);
             
            $empresa = new EmpresaModel();
            // escapes single and double quotes
            $str = addslashes($_POST['str']);
            //$etiqueta = addslashes($_POST['etiqueta']);
            $resultados = $empresa->like('NOMBRE',$str)->findAll();

            // echo a list where each li has a set_activity function bound to its onclick() event
            $empresas = [];
            foreach ($resultados as $empresa) {
                $empresas[$empresa->CIF] = $empresa->NOMBRE;
            }
            if (!empty($empresas)) {
                echo form_dropdown("cifempresa", $empresas, $empresa->CIF, ['class' => 'col-6 input-sm', 'size' => 6, 'id' => "cifempresa"]);
                $accion = <<< JS
               <script>        
                $("#cifempresa").on('click', function() {
                    var valor= $("#cifempresa option:selected").val();
                    //$("#uid_e").val(valor);
                });       
                </script>
    JS;
                echo $accion;
            } else {
                echo form_label('sin coincidencias', 'null', ['class' => 'form-control']);
            }
        }

    public function actualiza($ID_CT = NULL){
        helper(['form']);
        $centrotrabajo = new CentroTrabajoModel();
        $data['ID_CT'] = $ID_CT;
        $action = $ID_CT==NULL ? 'Añade uno nuevo' : 'Edita uno ya existente';
        $data['titulo'] = "$action centrotrabajo";
        if ($this->request->getMethod() == "post") { //viene de un formulario
            $data['centrotrabajo'] = $this->request->getPost();  //cogemos los valores de los campos
            unset($data['centrotrabajo']['busca']);
            $data['centrotrabajo']['CIF_EMP']=$data['centrotrabajo']['cifempresa'];
            unset($data['centrotrabajo']['cifempresa']);
            if ($ID_CT!==NULL) {
                //quitar la restricción de clave duplicada
               $centrotrabajo->setValidationRule('ID_CT', 'required|numeric');
                $data['centrotrabajo']['ID_CT'] = $ID_CT;   //estamos editando y recuperamos el $id del parámetro
                 if ($centrotrabajo->update($ID_CT, $data['centrotrabajo']) === false) {
                     
                    $data['errores'] = $centrotrabajo->errors();
                    return view('formularioseditar/centrotrabajoeditform', $data);
                } else {
                    return redirect()->to(site_url('TablaCTController'));
                }
            } else {
                $centrotrabajo->setValidationRule('ID_CT', 'required|numeric|is_unique[c_trabajo.ID_CT]');
               if ($centrotrabajo->insert($data['centrotrabajo']) === false) {
                    $data['errores'] = $centrotrabajo->errors();
                    return view('formularioseditar/centrotrabajoeditform', $data);
                } else {
                    return redirect()->to(site_url('TablaCTController'));
                }
            }
            
            /*echo '<pre>';
            print_r($data['CT']);
            echo '</pre>';*/
        } else { //else viene de una URL
            $data['centrotrabajo'] = $centrotrabajo->getCentroTrabajo(['ID_CT'=>$ID_CT]);
            if ($data['centrotrabajo']==FALSE){ //el alumno buscado no se encuentra
               $data ['ID_CT'] = NULL; 
               $data['centrotrabajo'] = [
                "ID_CT" => "",
                "NOMBRE" => "",
                "PROVINCIA" => "",
                "DIRECCION" => "",
                "TLF" => "",
                "CIF_EMP" => "",
            ];
            } 
            return view("formularioseditar/centrotrabajoeditform" ,$data);
        }   
       
    }
    
  public function eliminar($ID_CT){
   //   if ($this->auth->loggedIn() AND $this->auth->isAdmin()){
          $CentroTrabajoModel = new CentroTrabajoModel();
          $CentroTrabajoModel->delete($ID_CT);
          return redirect()->to('TablaCTController');
     // } else {
      // echo "no tiene permiso";
      //}
  }
}
    