<?php

    //Ésta es la carpeta dónde se almacena el nuevo Controlador.
    namespace App\Controllers;

    //Aqui lo seleccionamos para enlazarlo con el Modelo.
    use App\Models\InstructorModel;

    /* Después añadimos la clase con el nuevo controlador
    y lo enlazamos con la base de datos 'centro' y a su vez la tabla 'alumnos'.
    También se tiene que enlazar a su vez con el Modelo y la Vista que hemos creado. */
    class TablaInstructorController extends BaseController {

        public function index() 
        {
           $instructores = new InstructorModel();
           $datos ['instructores'] = $instructores->findAll();
           echo view('tablas/instructortabla', $datos);
        }
        //public function alumnos_grupo($valor="2CFSS") 
         //{ 
            //$alumnos = new AlumnosModel();
            //$datos ['titulo'] = "El título que sea";
            //$datos ['alumnos'] = $alumnos->select('alumnos.nombre, apellido1, apellido2, email, alumnos.NIA, matricula.grupo')
                //->join('matricula', 'alumnos.NIA = matricula.NIA','LEFT')
                //->where(['grupo' => $valor])
                //->findAll();
             //echo view('alumnos/listaalumnosgrupo', $datos);
             
    //}
    public function actualiza($DNI_INS = NULL){
        helper(['form']);
        $instructor = new InstructorModel();
        $data['DNI_INS'] = $DNI_INS;
        $action = $DNI_INS===NULL ? 'Añade uno nuevo' : 'Edita uno ya existente';
        $data['titulo'] = "$action instructor";
        if ($this->request->getMethod() == "post") { //viene de un formulario
            $data['instructor'] = $this->request->getPost();  //cogemos los valores de los campos
            if ($DNI_INS!==NULL) {
                $data['instructor']['DNI_INS'] = $DNI_INS;   //estamos editando y recuperamos el $id del parámetro
            }
            if ($instructor->save($data['instructor']) === false) {
                    $data['errors'] = $instructor->errors();
                    return view('formularios/instrcutorform', $data);
            } else {
                    return redirect()->to(site_url('TablaInstructorController'));
            }
        } else { //else viene de una URL
            $data['instructor'] = $instructor->getInstructor(['DNI_INS'=>$DNI_INS]);
            if ($data['instructor']==FALSE){ //el alumno buscado no se encuentra
               $data ['DNI_INS'] = NULL; 
               $data['instructor'] = [
                "DNI_INS" => "",
                "EMAIL" => "",
                "NOMBRE" => "",
                "TLF" => "",
                "ID_FPD" => "",
            ];
            } 
            return view("formularioseditar/instructoreditform" ,$data);	
        }   
    }
  public function eliminar($DNI_INS){
   //   if ($this->auth->loggedIn() AND $this->auth->isAdmin()){
          $InstructorModel = new InstructorModel();
          $InstructorModel->delete($DNI_INS);
          return redirect()->to('TablaInstructorController');
     // } else {
      // echo "no tiene permiso";
      //}
  }
    

}
    