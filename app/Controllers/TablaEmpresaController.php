<?php

    //Ésta es la carpeta dónde se almacena el nuevo Controlador.
    namespace App\Controllers;

    //Aqui lo seleccionamos para enlazarlo con el Modelo.
    use App\Models\EmpresaModel;

    /* Después añadimos la clase con el nuevo controlador
    y lo enlazamos con la base de datos 'centro' y a su vez la tabla 'alumnos'.
    También se tiene que enlazar a su vez con el Modelo y la Vista que hemos creado. */
    class TablaEmpresaController extends BaseController {

        public function index() 
        {
           $empresas = new EmpresaModel();
           $datos ['empresas'] = $empresas->findAll();
           echo view('tablas/empresatabla', $datos);
        }
        //public function alumnos_grupo($valor="2CFSS") 
         //{ 
            //$alumnos = new AlumnosModel();
            //$datos ['titulo'] = "El título que sea";
            //$datos ['alumnos'] = $alumnos->select('alumnos.nombre, apellido1, apellido2, email, alumnos.NIA, matricula.grupo')
                //->join('matricula', 'alumnos.NIA = matricula.NIA','LEFT')
                //->where(['grupo' => $valor])
                //->findAll();
             //echo view('alumnos/listaalumnosgrupo', $datos);
             
    //}
    public function actualiza($CIF = NULL){
        helper(['form']);
        $empresas= new EmpresaModel();
        $data['CIF'] = $CIF;
        $action = $CIF===NULL ? 'Añade una nueva' : 'Edita una ya existente';
        $data['titulo'] = "$action empresa";
        if ($this->request->getMethod() == "post") { //viene de un formulario
            $data['empresa'] = $this->request->getPost();  //cogemos los valores de los campos
            if ($CIF!==NULL) {
                $data['empresa']['CIF'] = $CIF;   //estamos editando y recuperamos el $id del parámetro
            }
            if ($empresas->save($data['empresa']) === false) {
                    $data['errors'] = $empresas->errors();
                    return view('formularios/empresaform', $data);
            } else {
                    return redirect()->to(site_url('TablaEmpresaController'));
            }
        } else { //else viene de una URL
            $data['empresa'] = $empresas->getEmpresa(['CIF'=>$CIF]);
            if ($data['empresa']==FALSE){ //el alumno buscado no se encuentra
               $data ['CIF'] = NULL; 
               $data['empresa'] = [
                "CIF" => "",
                "NOMBRE" => "",
                "PROVINCIA" => "",
                "PAIS" => "",
                "DIRECCION" => "",
                "POBLACION" => "",
                "CP" => "",
                "EMAIL" => "",
                "PDEMAIL" => "",
                "REPRESENTANTE" => "",
                "NIF_REPRESENTANTE" => "",
            ];
            } 
            return view("formularioseditar/empresaeditform" ,$data);
                    
            
        }   
    }
  public function eliminar($CIF){
   //   if ($this->auth->loggedIn() AND $this->auth->isAdmin()){
          $EmpresaModel = new EmpresaModel();
          $EmpresaModel->delete($CIF);
          return redirect()->to('TablaEmpresaController');
     // } else {
      // echo "no tiene permiso";
      //}
  }
    

}
    