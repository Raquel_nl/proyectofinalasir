<?php

namespace App\Controllers;
use App\Models\ConvenioModel;

class FormConvenioController extends BaseController{
    
    public function index(){
        helper('form');
        $data = new FormConvenioController();
        echo view('formularios/convenioform');
    }
    
    public function insertarconvenio() {
        helper('form');
        $data = new ConvenioModel();
        $convenio = [
            "nºconvenio" => $this->request->getPost('nºconvenio'),
            "caducidad" => $this->request->getPost('caducidad'),
            "representante" => $this->request->getPost('representante'),
            "cif_emp" => $this->request->getPost('cif_emp'),
            "empresa" => $this->request->getPost('empresa'),
            "fecha" => $this->request->getPost('fecha'),
            
        ];

        $data->insert($convenio);
        //print_r($convenio);
        echo " ";
        return redirect()->to(site_url('TablaConvenioController'));
}}