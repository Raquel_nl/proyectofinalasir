<?php

namespace App\Controllers;
use App\Models\CentroTrabajoModel;

class FormCTController extends BaseController{
    
    public function index(){
        helper('form');
        //$data = new FormCTController();
        echo view('formularios/centrotrabajoform');
    }
    
    public function insertarcentrotrabajo() {
        helper('form');
        $data = new CentroTrabajoModel();
        $centrotrabajo = [
            "ID_CT" => $this->request->getPost('ID_CT'),
            "NOMBRE" => $this->request->getPost('NOMBRE'),
            "PROVINCIA" => $this->request->getPost('provincia'),
            "DIRECCION" => $this->request->getPost('DIRECCION'),
            "TLF" => $this->request->getPost('TLF'),
            "CIF_EMP" => $this->request->getPost('CIF_EMP'),
            "ID_FPD" => $this->request->getPost('ID_FPD'),
            
        ];

        $data->insert($centrotrabajo);
        //print_r($centrotrabajo);
        echo " ";
        return redirect()->to(site_url('TablaCTController'));
}}