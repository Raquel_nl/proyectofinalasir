<?php

namespace App\Controllers;
use App\Models\FPDualModel;

class FormFPDualController extends BaseController{
    
    public function index(){
        helper('form');
        $data = new FormFPDualController();
        echo view('formularios/fpdualform');
    }
    
    public function insertarfpdual() {
        helper('form');
        $data = new FPDualModel();
        $fpdual = [
            "ID_CT" => $this->request->getPost('ID_CT'),
            "IMPORTE" => $this->request->getPost('IMPORTE'),
            "H_TOTALES" => $this->request->getPost('H_TOTALES'),
            "H_EMPRESA" => $this->request->getPost('H_EMPRESA'),
            "H_CENTRO" => $this->request->getPost('H_CENTRO'),
            "DNI_ALU" => $this->request->getPost('DNI_ALU'),
            "FECHA_INI" => $this->request->getPost('FECHA_INI'),
            "FECHA_FIN" => $this->request->getPost('FECHA_FIN'),
            "nºconvenio" => $this->request->getPost('nºconvenio'),
            
        ];

        $data->insert($fpdual);
        //print_r($centrotrabajo);
        echo " ";
        return redirect()->to(site_url('TablaFPDualController'));
}}