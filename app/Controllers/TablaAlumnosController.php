<?php

    namespace App\Controllers;
    use App\Models\AlumnosModel;

    class TablaAlumnosController extends BaseController {

        public function index() 
        {
           $alumnos = new AlumnosModel();
           $datos ['alumnos'] = $alumnos->findAll();
           echo view('tablas/alumnostabla', $datos);
        }
     
    public function actualiza($DNI_ALU = NULL){
        helper(['form']);
        $alumnos = new AlumnosModel();
        $data['DNI_ALU'] = $DNI_ALU;
        $action = $DNI_ALU===NULL ? 'Añade uno nuevo' : 'Edita uno ya existente';
        $data['titulo'] = "$action alumno";
        if ($this->request->getMethod() == "post") { 
            $data['alumno'] = $this->request->getPost(); 
            if ($DNI_ALU!==NULL) {
                $data['alumno']['DNI_ALU'] = $DNI_ALU; 
            }
            if ($alumnos->save($data['alumno']) === false) {
                    $data['errors'] = $alumnos->errors();
                    return view('formularios/alumnosform', $data);
            } else {
                    return redirect()->to(site_url('TablaAlumnosController'));
            }
        } else { 
            $data['alumno'] = $alumnos->getAlumno(['DNI_ALU'=>$DNI_ALU]);
            if ($data['alumno']==FALSE){ 
               $data ['DNI_ALU'] = NULL; 
               $data['alumno'] = [
                "DNI_ALU" => "",
                "EMAIL" => "",
                "NOMBRE" => "",
                "APELLIDO1" => "",
                "APELLIDO2" => "",
                "TLF" => "",
            ];
            } 
            return view("formularioseditar/alumnoseditform" ,$data);
        }   
    }
       
  public function eliminar($DNI_ALU){
          $AlumnosModel = new AlumnosModel();
          $AlumnosModel->delete($DNI_ALU);
          return redirect()->to('TablaAlumnosController');
 
  }
    

}
