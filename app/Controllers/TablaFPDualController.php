<?php

    //Ésta es la carpeta dónde se almacena el nuevo Controlador.
    namespace App\Controllers;

    //Aqui lo seleccionamos para enlazarlo con el Modelo.
    use App\Models\FPDualModel;

    /* Después añadimos la clase con el nuevo controlador
    y lo enlazamos con la base de datos 'centro' y a su vez la tabla 'alumnos'.
    También se tiene que enlazar a su vez con el Modelo y la Vista que hemos creado. */
    class TablaFPDualController extends BaseController {

        public function index() 
        {
           $fpduales = new FPDualModel();
           $datos ['fpduales'] = $fpduales->findAll();
           echo view('tablas/fpdualtabla', $datos);
        }
        //public function alumnos_grupo($valor="2CFSS") 
         //{ 
            //$alumnos = new AlumnosModel();
            //$datos ['titulo'] = "El título que sea";
            //$datos ['alumnos'] = $alumnos->select('alumnos.nombre, apellido1, apellido2, email, alumnos.NIA, matricula.grupo')
                //->join('matricula', 'alumnos.NIA = matricula.NIA','LEFT')
                //->where(['grupo' => $valor])
                //->findAll();
             //echo view('alumnos/listaalumnosgrupo', $datos);
             
    //}
    public function actualiza($ID_FPD = NULL){
        helper(['form']);
        $fpdual= new FPDualModel();
        $data['ID_FPD'] = $ID_FPD;
        $action = $ID_FPD===NULL ? 'Añade uno nuevo' : 'Edita uno ya existente';
        $data['titulo'] = "$action fpdual";
        if ($this->request->getMethod() == "post") { //viene de un formulario
            $data['fpdual'] = $this->request->getPost();  //cogemos los valores de los campos
            if ($ID_FPD!==NULL) {
                $data['fpdual']['ID_FPD'] = $ID_FPD;   //estamos editando y recuperamos el $id del parámetro
            }
            if ($fpdual->save($data['fpdual']) === false) {
                    $data['errors'] = $fpdual->errors();
                    return view('formularios/fpdualform', $data);
            } else {
                    return redirect()->to(site_url('TablaFPDualController'));
            }
        } else { //else viene de una URL
            $data['fpdual'] = $fpdual->getFPDual(['ID_FPD'=>$ID_FPD]);
            if ($data['fpdual']==FALSE){ //el alumno buscado no se encuentra
               $data ['ID_FPD'] = NULL; 
               $data['fpdual'] = [
                "ID_FPD" => "",
                "IMPORTE" => "",
                "H_TOTALES" => "",
                "H_EMPRESA" => "",
                "H_CENTRO" => "",
                "DNI_ALU" => "",
                "FECHA_INI" => "",
                "FECHA_FIN" => "",
                "NºCONVENIO" => "",
            ];
            } 
            return view("formularioseditar/fpdualeditform" ,$data);
            }   
    }
  public function eliminar($ID_FPD){
   //   if ($this->auth->loggedIn() AND $this->auth->isAdmin()){
          $FPDualModel = new FPDualModel();
          $FPDualModel->delete($ID_FPD);
          return redirect()->to('TablaFPDualController');
     // } else {
      // echo "no tiene permiso";
      //}
  }
    

}
    