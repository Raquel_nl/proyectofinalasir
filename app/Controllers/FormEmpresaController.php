<?php

namespace App\Controllers;
use App\Models\EmpresaModel;

class FormEmpresaController extends BaseController{
    
    public function index(){
        helper('form');
        $data = new FormEmpresaController();
        echo view('formularios/empresaform');
    }
    
    public function insertarempresa() {
        helper('form');
        $data = new EmpresaModel();
        $empresa = [
            "CIF" => $this->request->getPost('CIF'),
            "NOMBRE" => $this->request->getPost('Nombre'),
            "PROVINCIA" => $this->request->getPost('PROVINCIA'),
            "PAIS" => $this->request->getPost('PAIS'),
            "DIRECCION" => $this->request->getPost('DIRECCION'),
            "POBLACION" => $this->request->getPost('POBLACION'),
            "TLF" => $this->request->getPost('TLF'),
            "CP" => $this->request->getPost('CP'),
            "EMAIL" => $this->request->getPost('EMAIL'),
            "PDEMAIL" => $this->request->getPost('PDEMAIL'),
            "REPRESENTANTE" => $this->request->getPost('REPRESENTANTE'),
            "NIF_REPRESENTANTE" => $this->request->getPost('NIF_REPRESENTANTE'),
            
        ];

        $data->insert($empresa);
        //print_r($empresa);
       echo " ";
       return redirect()->to(site_url('TablaEmpresaController'));
    }
    
 
    }
 
