<?php

    //Ésta es la carpeta dónde se almacena el nuevo Controlador.
    namespace App\Controllers;

    //Aqui lo seleccionamos para enlazarlo con el Modelo.
    use App\Models\ConvenioModel;

    /* Después añadimos la clase con el nuevo controlador
    y lo enlazamos con la base de datos 'centro' y a su vez la tabla 'alumnos'.
    También se tiene que enlazar a su vez con el Modelo y la Vista que hemos creado. */
    class TablaConvenioController extends BaseController {

        public function index() 
        {
           $convenios = new ConvenioModel();
           $datos ['convenios'] = $convenios->findAll();
           echo view('tablas/conveniotabla', $datos);
        }
        //public function alumnos_grupo($valor="2CFSS") 
         //{ 
            //$alumnos = new AlumnosModel();
            //$datos ['titulo'] = "El título que sea";
            //$datos ['alumnos'] = $alumnos->select('alumnos.nombre, apellido1, apellido2, email, alumnos.NIA, matricula.grupo')
                //->join('matricula', 'alumnos.NIA = matricula.NIA','LEFT')
                //->where(['grupo' => $valor])
                //->findAll();
             //echo view('alumnos/listaalumnosgrupo', $datos);
             
    //}
    public function actualiza($nºconvenio = NULL){
        helper(['form']);
        $convenio = new ConvenioModel();
        $data['nºconvenio'] = $nºconvenio;
        $action = $nºconvenio===NULL ? 'Añade uno nuevo' : 'Edita uno ya existente';
        $data['titulo'] = "$action convenio";
        if ($this->request->getMethod() == "post") { //viene de un formulario
            $data['convenio'] = $this->request->getPost();  //cogemos los valores de los campos
            if ($nºconvenio!==NULL) {
                $data['convenio']['nºconvenio'] = $nºconvenio;   //estamos editando y recuperamos el $id del parámetro
            }
            if ($convenio->save($data['convenio']) === false) {
                    $data['errors'] = $convenio->errors();
                    return view('formularios/convenioform', $data);
            } else {
                    return redirect()->to(site_url('TablaConvenioController'));
            }
        } else { //else viene de una URL
            $data['convenio'] = $convenio->getConvenio(['nºconvenio'=>$nºconvenio]);
            if ($data['convenio']==FALSE){ //el alumno buscado no se encuentra
               $data ['nºconvenio'] = NULL; 
               $data['convenio'] = [
                "nºconvenio" => "",
                "caducidad" => "",
                "representante" => "",
                "cif_emp" => "",
                "empresa" => "",
                "fecha" => "",
            ];
            } 
            return view("formularioseditar/convenioeditform" ,$data); 	
        }   
    }
  public function eliminar($nºconvenio){
   //   if ($this->auth->loggedIn() AND $this->auth->isAdmin()){
          $ConvenioModel = new ConvenioModel();
          $ConvenioModel->delete($nºconvenio);
          return redirect()->to('TablaConvenioController');
     // } else {
      // echo "no tiene permiso";
      //}
  }
    

}
    