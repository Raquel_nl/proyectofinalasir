<?php

    //Ésta es la carpeta dónde se almacena el nuevo Controlador.
    namespace App\Controllers;

    //Aqui lo seleccionamos para enlazarlo con el Modelo.
    use App\Models\TutorModel;

    /* Después añadimos la clase con el nuevo controlador
    y lo enlazamos con la base de datos 'centro' y a su vez la tabla 'alumnos'.
    También se tiene que enlazar a su vez con el Modelo y la Vista que hemos creado. */
    class TablaTutorController extends BaseController {

        public function index() 
        {
           $tutores = new TutorModel();
           $datos ['tutores'] = $tutores->findAll();
           echo view('tablas/tutortabla', $datos);
        }
   
        
        
    public function actualiza($CIF_EMP = NULL){
        helper(['form']);
        $tutor = new TutorModel();
        $data['CIF_EMP'] = $CIF_EMP;
        $action = $CIF_EMP===NULL ? 'Añade uno nuevo' : 'Edita uno ya existente';
        $data['titulo'] = "$action tutor";
        if ($this->request->getMethod() == "post") { //viene de un formulario
            $data['tutor'] = $this->request->getPost();  //cogemos los valores de los campos
            if ($CIF_EMP!==NULL) {
                $data['tutor']['CIF_EMP'] = $CIF_EMP;   //estamos editando y recuperamos el $id del parámetro
            }
            if ($tutor->save($data['tutor']) === false) {
                    $data['errors'] = $tutor->errors();
                    return view('formularios/tutorform', $data);
            } else {
                    return redirect()->to(site_url('TablaTutorController'));
            }
        } else { //else viene de una URL
            $data['tutor'] = $tutor->getTutor(['CIF_EMP'=>$CIF_EMP]);
            if ($data['tutor']==FALSE){ //el alumno buscado no se encuentra
               $data ['CIF_EMP'] = NULL; 
               $data['tutor'] = [
                "CCIF_EMP" => "",
                "DNI_INS" => "",
            ];
            } 
            return view("formularioseditar/tutoreditform" ,$data);
        }   
    }
    
   
  public function eliminar($CIF_EMP){
   //   if ($this->auth->loggedIn() AND $this->auth->isAdmin()){
          $TutorModel = new TutorModel();
          $TutorModel->delete($CIF_EMP);
          return redirect()->to('TablaInstructorController');
     // } else {
      // echo "no tiene permiso";
      //}
  }

}
    