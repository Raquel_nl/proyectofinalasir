<?= $this->extend('layout/layout') ?>
<?= $this->section('content') ?>
<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

        <title>Añadir_fpdual</title>
    </head>

    <style>

        body {

            background-color:#E6E6FA;
        }

        input:hover {

            background-color: #ffffff;
            transform: scale(1.2);

        }

        div {

            margin: auto;
        }



    </style>
    <body>
        <br>
        <h3 style="text-align: center;">AÑADIR FP DUAL</h3>
        <div class="container" id="alineacion">
            <form method="post" action="<?= base_url("/index.php/FormFPDualController/insertarfpdual/") ?>">
               <div class="form-group row">
            <div>
                <span id="ID_FPD">ID FP Dual</span>
                <?= form_input('ID_FPD', set_value('ID_FPD'), ['placeholder' => "Inserta el ID", 'required' => "required", 'class' => "form-control", 'maxlength' => '8',]) ?>    
            </div>
                    </div><div class="form-group row">
            <div>
                <span id="IMPORTE">Importe FP Dual</span>
                <?= form_input('IMPORTE', set_value('IMPORTE'), ['placeholder' => "Inserta el importe", 'required' => "required", 'class' => "form-control", 'maxlength' => '100',]) ?>    
            </div>
                    </div><div class="form-group row">
            <div>
                <span id="H_TOTALES">Horas Totales</span>
                <?= form_input('H_TOTALES', set_value('H_TOTALES'), ['placeholder' => "Inserta las horas totales de la FP Dual", 'required' => "required", 'class' => "form-control", 'maxlength' => '20',]) ?>    
            </div>
                    </div>
                    
                    <div class="form-group row">
            <div>
                <span id="H_EMPRESA">Horas en la Empresa</span>
                <?= form_input('H_EMPRESA', set_value('H_EMPRESA'), ['placeholder' => "Inserta las horas en la empresa", 'required' => "required", 'class' => "form-control", 'maxlength' => '20',]) ?>    
            </div>
                    </div>
        <div class="form-group row">
            <div>
                <span id="H_CENTRO">Horas en el Centro</span>
                <?= form_input('H_CENTRO', set_value('H_CENTRO'), ['placeholder' => "Inserta las horas en el centro", 'required' => "required", 'class' => "form-control", 'maxlength' => '100',]) ?>    
            </div>
        </div>
        <div class="form-group row">
            <div>
                <span id="DNI_ALU">DNI del Alumno</span>
                <?= form_input('DNI_ALU', set_value('DNI_ALU'), ['placeholder' => "Inserta el DNI del alumno", 'required' => "required", 'class' => "form-control", 'maxlength' => '10',]) ?>    
            </div>
                    </div> 
        <div class="form-group row">
            <div>
                <span id="FECHA_INI">Fecha de Inicio</span>
                <?= form_input('FECHA_INI', set_value('FECHA_INI'), ['placeholder' => "Inserta la fecha de inicio", 'required' => "required", 'class' => "form-control", 'maxlength' => '9',]) ?>    
            </div>
                    </div>     
        <div class="form-group row">
            <div>
                <span id="FECHA_FIN">Fecha de Fin</span>
                <?= form_input('FECHA_FIN', set_value('FECHA_FIN'), ['placeholder' => "Inserta la fecha de fin", 'required' => "required", 'class' => "form-control", 'maxlength' => '5',]) ?>    
            </div>
                    </div> 
        <div class="form-group row">
            <div>
                <span id="nºconvenio">Número de convenio</span>
                <?= form_input('nºconvenio', set_value('nºconvenio'), ['placeholder' => "Inserta el número de convenio", 'required' => "required", 'class' => "form-control", 'maxlength' => '20',]) ?>    
            </div>
                    </div>
      
<button type="submit" class="btn btn-secondary" style="margin-left:46%;"><i class="fas fa-upload"></i>&nbsp; Enviar</button>
            </form>
        </div>
    </body>
</html>

<?= $this->endSection() ?>