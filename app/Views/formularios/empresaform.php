<?= $this->extend('layout/layout') ?>
<?= $this->section('content') ?>
<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

        <title>Añadir_empresa</title>
    </head>

    <style>

        body {

            background-color:#E6E6FA;
        }

        input:hover {

            background-color: #ffffff;
            transform: scale(1.2);

        }

        div {

            margin: auto;
        }



    </style>
    <body>
        <br>
        <h3 style="text-align: center;">AÑADIR EMPRESA</h3>
        <div class="container" id="alineacion">
            <form method="post" action="<?= base_url("/index.php/FormEmpresaController/insertarempresa/") ?>">
               <div class="form-group row">
            <div>
                <span id="CIF">CIF</span>
                <?= form_input('CIF', set_value('CIF'), ['placeholder' => "Inserta el CIF", 'required' => "required", 'class' => "form-control", 'maxlength' => '8',]) ?>    
            </div>
                    </div><div class="form-group row">
            <div>
                <span id="NOMBRE">Nombre</span>
                <?= form_input('Nombre', set_value('Nombre'), ['placeholder' => "Inserta el nombre de la empresa", 'required' => "required", 'class' => "form-control", 'maxlength' => '100',]) ?>    
            </div>
                    </div><div class="form-group row">
            <div>
                <span id="PROVINCIA">Provincia</span>
                <?= form_input('PROVINCIA', set_value('PROVINCIA'), ['placeholder' => "Inserta la provincia", 'required' => "required", 'class' => "form-control", 'maxlength' => '20',]) ?>    
            </div>
                    </div>
                    
                    <div class="form-group row">
            <div>
                <span id="PAIS">País</span>
                <?= form_input('PAIS', set_value('PAIS'), ['placeholder' => "Inserta el País", 'required' => "required", 'class' => "form-control", 'maxlength' => '20',]) ?>    
            </div>
                    </div>
        <div class="form-group row">
            <div>
                <span id="DIRECCION">Dirección</span>
                <?= form_input('DIRECCION', set_value('DIRECCION'), ['placeholder' => "Inserta la dirección", 'required' => "required", 'class' => "form-control", 'maxlength' => '100',]) ?>    
            </div>
        </div>
        <div class="form-group row">
            <div>
                <span id="POBLACION">Población</span>
                <?= form_input('POBLACION', set_value('POBLACION'), ['placeholder' => "Inserta la población", 'required' => "required", 'class' => "form-control", 'maxlength' => '10',]) ?>    
            </div>
                    </div> 
        <div class="form-group row">
            <div>
                <span id="TLF">Teléfono</span>
                <?= form_input('TLF', set_value('TLF'), ['placeholder' => "Inserta el teléfono", 'required' => "required", 'class' => "form-control", 'maxlength' => '9',]) ?>    
            </div>
                    </div>     
        <div class="form-group row">
            <div>
                <span id="CP">Código Postal</span>
                <?= form_input('CP', set_value('CP'), ['placeholder' => "Inserta el Código Postal", 'required' => "required", 'class' => "form-control", 'maxlength' => '5',]) ?>    
            </div>
                    </div> 
        <div class="form-group row">
            <div>
                <span id="EMAIL">E-Mail</span>
                <?= form_input('EMAIL', set_value('EMAIL'), ['placeholder' => "Inserta el E-Mail", 'required' => "required", 'class' => "form-control", 'maxlength' => '20',]) ?>    
            </div>
                    </div>
        <div class="form-group row">
            <div>
                <span id="PDEMAIL">E-Mail de Protección de Datos</span>
                <?= form_input('PDEMAIL', set_value('PDEMAIL'), ['placeholder' => "Inserta el E-Mail de Protección de Datos", 'required' => "required", 'class' => "form-control", 'maxlength' => '20',]) ?>    
            </div>
                    </div> 
        <div class="form-group row">
            <div>
                <span id="REPRESENTANTE">Representante de la empresa</span>
                <?= form_input('REPRESENTANTE', set_value('REPRESENTANTE'), ['placeholder' => "Inserta el representante de la empresa", 'required' => "required", 'class' => "form-control", 'maxlength' => '20',]) ?>    
            </div>
                    </div> 
        <div class="form-group row">
            <div>
                <span id="NIF_REPRESENTANTE">NIF del Representante</span>
                <?= form_input('NIF_REPRESENTANTE', set_value('NIF_REPRESENTANTE'), ['placeholder' => "Inserta el NIF del representante", 'required' => "required", 'class' => "form-control", 'maxlength' => '9',]) ?>    
            </div>
                    </div>
<button type="submit" class="btn btn-secondary" style="margin-left:46%;"><i class="fas fa-upload"></i>&nbsp; Añadir</button>
            </form>
        </div>
    </body>
</html>
<?= $this->endSection() ?>