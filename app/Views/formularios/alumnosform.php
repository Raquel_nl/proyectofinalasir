<?= $this->extend('layout/layout') ?>
<?= $this->section('content') ?>
<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

        <title>Añadir_alumno</title>
    </head>

    <style>

        body {

            background-color:#E6E6FA;
        }

        input:hover {

            background-color: #ffffff;
            transform: scale(1.2);

        }

        div {

            margin: auto;
        }



    </style>
    <body>
        <br>
        <h3 style="text-align: center;">AÑADIR ALUMNO</h3>
        <div class="container" id="alineacion">                 
            <form method="post" action="<?= base_url("/index.php/FormAlumnosController/insertaralumno/") ?>">
                <div class="form-group row">                    
                    <div>
                        <span id="DNI_ALU">DNI Alumno</span>
                        <?= form_input('DNI_ALU', set_value('DNI_ALU'), ['placeholder' => "Inserta el DNI", 'required' => "required", 'class' => "form-control", 'maxlength' => '9',]) ?>    
                    </div>
                </div>
                <div class="form-group row">
                    <div>
                        <span id="NOMBRE">Nombre</span>
                        <?= form_input('NOMBRE', set_value('NOMBRE'), ['placeholder' => "Inserta el Nombre", 'required' => "required", 'class' => "form-control", 'maxlength' => '100',]) ?>    
                    </div>
                </div>
                <div class="form-group row">
                    <div>
                        <span id="APELLIDO1">Primer Apellido</span>
                        <?= form_input('APELLIDO1', set_value('APELLIDO1'), ['placeholder' => "Inserta el primer apellido", 'required' => "required", 'class' => "form-control", 'maxlength' => '20',]) ?>    
                    </div>
                </div>    
                <div class="form-group row">
                    <div>
                        <span id="APELLIDO2">Segundo Apellido</span>
                        <?= form_input('APELLIDO2', set_value('APELLIDO2'), ['placeholder' => "Inserta el segundo apellido", 'required' => "required", 'class' => "form-control", 'maxlength' => '20',]) ?>    
                    </div>
                </div>
                <div class="form-group row">
                    <div>
                        <span id="EMAIL">e-mail</span>
                        <?= form_input('EMAIL', set_value('EMAIL'), ['placeholder' => "Inserta el E-Mail", 'required' => "required", 'class' => "form-control", 'maxlength' => '100',]) ?>    
                    </div>
                </div>
                <div class="form-group row">
                    <div>
                        <span id="TLF">Teléfono de contacto</span>
                        <?= form_input('TLF', set_value('TLF'), ['placeholder' => "Inserta el teléfono", 'required' => "required", 'class' => "form-control", 'maxlength' => '9',]) ?>    
                    </div>
                </div>                                      
                <button type="submit" class="btn btn-secondary" style="margin-left:46%;"><i class="fas fa-upload"></i>&nbsp; Enviar</button>
            </form>
        </div>
    </body>
</html>
<?= $this->endSection() ?>