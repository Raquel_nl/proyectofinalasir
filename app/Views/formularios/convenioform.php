<?= $this->extend('layout/layout') ?>
<?= $this->section('content') ?>
<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

        <title>Añadir_convenio</title>
    </head>

    <style>

        body {

            background-color:#E6E6FA;
        }

        input:hover {

            background-color: #ffffff;
            transform: scale(1.2);

        }

        div {

            margin: auto;
        }



    </style>
    <body>
        <br>
        <h3 style="text-align: center;">AÑADIR CONVENIO</h3>
        <div class="container" id="alineacion">
            <form method="post" action="<?= base_url("/index.php/FormConvenioController/insertarconvenio") ?>">
               <div class="form-group row">
            <div>
                <span id="nºconvenio">Nº Convenio</span>
                <?= form_input('nºconvenio', set_value('nºconvenio'), ['placeholder' => "Inserta el n.º de convenio", 'required' => "required", 'class' => "form-control", 'maxlength' => '8',]) ?>    
            </div>
                    </div><div class="form-group row">
            <div>
                <span id="caducidad">Fecha de caducidad</span>
                <?= form_input('caducidad', set_value('caducidad'), ['placeholder' => "Inserta la fecha de caducidad", 'required' => "required", 'class' => "form-control", 'maxlength' => '100',]) ?>    
            </div>
                    </div><div class="form-group row">
            <div>
                <span id="representante">Nombre del Representante</span>
                <?= form_input('representante', set_value('representante'), ['placeholder' => "Inserta el nombre del representante", 'required' => "required", 'class' => "form-control", 'maxlength' => '20',]) ?>    
            </div>
                    </div>
                    
                    <div class="form-group row">
            <div>
                <span id="cif_emp">CIF de la empresa</span>
                <?= form_input('cif_emp', set_value('cif_emp'), ['placeholder' => "Inserta el CIF de la empresa", 'required' => "required", 'class' => "form-control", 'maxlength' => '20',]) ?>    
            </div>
                    </div>
        <div class="form-group row">
            <div>
                <span id="empresa">Nombre de la empresa</span>
                <?= form_input('empresa', set_value('empresa'), ['placeholder' => "Inserta el nombre de la empresa", 'required' => "required", 'class' => "form-control", 'maxlength' => '100',]) ?>    
            </div>
        </div>
        <div class="form-group row">
            <div>
                <span id="fecha">Fecha de convenio</span>
                <?= form_input('fecha', set_value('fecha'), ['placeholder' => "Inserta la fecha del convenio", 'required' => "required", 'class' => "form-control", 'maxlength' => '10',]) ?>    
            </div>
                    </div>
                    
<button type="submit" class="btn btn-secondary" style="margin-left:46%;"><i class="fas fa-upload"></i>&nbsp; Enviar</button>
            </form>
        </div>
    </body>
</html>

<?= $this->endSection() ?>