<?= $this->extend('layout/layout') ?>
<?= $this->section('content') ?>
<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

        <title>Añadir_tutor</title>
    </head>

    <style>

        body {

            background-color:#E6E6FA;
        }

        input:hover {

            background-color: #ffffff;
            transform: scale(1.2);

        }

        div {

            margin: auto;
        }



    </style>
    <body>
        <br>
        <h3 style="text-align: center;">AÑADIR TUTOR</h3>
        <div class="container" id="alineacion">
            <form method="post" action="<?= base_url("/index.php/FormTutorController/insertartutor") ?>">
               <div class="form-group row">
            <div>
                <span id="DNI_INS">DNI Instructor</span>
                <?= form_input('DNI_INS', set_value('DNI_INS'), ['placeholder' => "Inserta el DNI", 'required' => "required", 'class' => "form-control", 'maxlength' => '8',]) ?>    
            </div>
                    </div><div class="form-group row">
            <div>
                <span id="CIF_EMP">CIF Empresa</span>
                <?= form_input('CIF_EMP', set_value('CIF_EMP'), ['placeholder' => "Inserta el CIF de la empresa", 'required' => "required", 'class' => "form-control", 'maxlength' => '100',]) ?>    
            </div>
                    </div>

<button type="submit" class="btn btn-secondary" style="margin-left:46%;"><i class="fas fa-upload"></i>&nbsp; Enviar</button>
            </form>
        </div>
    </body>
</html>

<?= $this->endSection() ?>



