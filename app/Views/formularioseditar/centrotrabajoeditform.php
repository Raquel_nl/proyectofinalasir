<?= $this->extend('layout/layout') ?>
<?= $this->section('content') ?>
<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

        <title>Editar_centrotrabajo</title>
    </head>

    <style>

        body {

            background-color:#E6E6FA;
        }

        input:hover {

            background-color: #ffffff;
            transform: scale(1.2);

        }



    </style>
    <body>
        <br>
        <h3 style="text-align: center;">AÑADIR CENTRO DE TRABAJO</h3>
        <div class="container" id="alineacion">
          <?php if  (isset($errores) ) 
             {print_r($errores);}
          ?>  
            <form method="post" action="<?= base_url("/index.php/TablaCTController/actualiza/".$centrotrabajo['ID_CT']) ?>">
               <div class="form-group row">
                    <div>
                        <span id="ID_CT">ID Centro Trabajo</span>
                        <?= form_input('ID_CT', set_value('ID_CT',$centrotrabajo['ID_CT']), ['placeholder' => "Inserta el ID", 'required' => "required", 'class' => "form-control", 'maxlength' => '8',]) ?>    
                    </div>
                </div>
                <div class="form-group row">
                    <div>
                        <span id="NOMBRE">Nombre</span>
                        <?= form_input('NOMBRE', set_value('NOMBRE',$centrotrabajo['NOMBRE']), ['placeholder' => "Inserta el nombre de la empresa", 'required' => "required", 'class' => "form-control", 'maxlength' => '100',]) ?>    
                    </div>
                </div>
                <div class="form-group row">
                    <div>
                        <span id="PROVINCIA">Provincia</span>
                        <?= form_input('PROVINCIA', set_value('PROVICNIA',$centrotrabajo['PROVINCIA']), ['placeholder' => "Inserta la provincia", 'required' => "required", 'class' => "form-control", 'maxlength' => '20',]) ?>    
                    </div>
                </div>
                <span id="CIF_EMP">CIF Empresa</span>
                
                <div class="form-row">
                    <div class="input-group col-6">
                        <div class="input-group-prepend">
                            <span class="input-group-text">
                                <span class="bi bi-search"></span>
                            </span>
                        </div>
                        
                        <?php echo form_input(['name' => 'busca', 'class' => 'form-control buscador', 'placeholder' => 'buscar la Empresa', 'id' => 'buscador']); ?>
                    </div>    
                    <div class="col-6">    
                        <div id="etiqueta">
                            <?php echo form_input('cifempresa', set_value('cifempresa', $centrotrabajo['CIF_EMP']), ['class' => 'form-control input', 'id' => 'cifempresa', 'readonly' => 'readonly']); ?>
                        </div> 
                    </div>    
                    <div class="col-6" id="cif_list">
                        <!-- Aquí pondremos el select con los valores que coincidan - -->
                    </div>  
                </div>
                
                <div class="form-group row">
                    <div>
                    <span id="DIRECCION">Dirección</span>
                    <?= form_input('DIRECCION', set_value('DIRECCION',$centrotrabajo['DIRECCION']), ['placeholder' => "Inserta la dirección", 'required' => "required", 'class' => "form-control", 'maxlength' => '100',]) ?>    
                    </div>
                </div>
                
                <div class="form-group row">
                    <div>
                    <span id="TLF">Teléfono</span>
                    <?= form_input('TLF', set_value('TLF',$centrotrabajo['TLF']), ['placeholder' => "Inserta el teléfono", 'required' => "required", 'class' => "form-control", 'maxlength' => '9',]) ?>    
                    </div>
                </div>    
            <button type="submit" class="btn btn-secondary" href="<?= site_url('TablaFPDualController') ?>" style="margin-left:46%;"><i class="fas fa-upload"></i>&nbsp; Enviar</button>
            </form>
        </div>
    </body>
</html>

<?= $this->endSection() ?>

<?= $this->section('javascript') ?>
<?= $this->include('formularioseditar/centrotrabajoeditform_js') ?>
<?= $this->endSection() ?>