<script>
//buscar empresas);     
function autosuggest_empresa(event) {
        // if there's no text to search, hide the list div
        var str = this.value;
        
        if (str.length === 0) {
            $('#cif_list').fadeOut(500);
            $('#etiqueta').show();
        } else {
            // Ajax request to CodeIgniter controller "ajax" method "autosuggest"
            // post the str parameter value
            $('#etiqueta').hide();
            $.post('/TablaCTController/busca_empresa/',
                    {'str': str},
                    function (result) {
                        // if there is a result, fill the list div, fade it in
                        // then remove the loading animation
                        if (result) {
                            $('#cif_list').html(result);
                            $('#cif_list').fadeIn(500);
                            var valor= $("#cifempresa"+" option:selected").val();
                            $('#uid_e_').val(valor);
                        }
                    });
                 }
    }
</script>

