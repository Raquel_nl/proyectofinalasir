<?= $this->extend('layout/layout') ?>
<?= $this->section('content') ?>
<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <title>LISTADO DE INSTRUCTORES</title>
    </head>

<style>

    body {
        
        background-color:#E6E6FA;
    }
    
    th {
        
        background-color:white;
    }
    
    tr {
        
        background-color:white;
    }
    
</style>
<body>
<h2 style="text-align: center;">LISTADO DE INSTRUCTORES</h2>
<div class="container">
<table class="table table-hover table-bordered">
<thead>
    <th class="th_class">DNI Instructor</th>
    <th class="th_class">E-Mail</th>
    <th class="th_class">Nombre</th>
    <th class="th_class">Teléfono</th>
    <th class="th_class">ID FP Dual</th>
    <th class="th_class">Acciones<a href="<?= site_url('FormInstructorController/')?>"class="btn btn-warning btn-sm">Añadir</a>
</thead>
<tbody>
    <?php $titulo ?>
    <?php foreach ($instructores as $instructor): ?>
    <tr>
        <td class="bottom"><?= $instructor['DNI_INS'] ?></td>
        <td class="bottom"><?= $instructor['EMAIL'] ?></td>
        <td class="bottom"><?= $instructor['NOMBRE'] ?></td>
        <td class="bottom"><?= $instructor['TLF'] ?></td>
        <td class="bottom"><?= $instructor['ID_FPD'] ?></td>
        <td class="bottom"><a href="<?= site_url('TablaInstructorController/eliminar/'.$instructor['DNI_INS'])?>" 
                              class="btn btn-danger btn-sm" onclick="return confirm('Estás seguro de borrar al instructor <?=$instructor['DNI_INS'] ?>')">Borrar</a>

                  
                  
                   
                  
                  <a href="<?= site_url('TablaInstructorController/actualiza/'.$instructor['DNI_INS'])?>"class="btn btn-primary btn-sm">Editar</a>
            
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
        
</div>
</body>
</html>
<?= $this->endSection() ?>