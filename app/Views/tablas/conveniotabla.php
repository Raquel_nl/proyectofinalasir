<?= $this->extend('layout/layout') ?>
<?= $this->section('content') ?>
<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <title>LISTADO DE CONVENIOS</title>
    </head>

<style>

    body {
        
        background-color:#E6E6FA;
    }
    
    th {
        
        background-color:white;
    }
    
    tr {
        
        background-color:white;
    }
    
</style>
<body>
<h2 style="text-align: center;">LISTADO DE CONVENIOS</h2>
<div class="container">
<table class="table table-hover table-bordered">
<thead>
    <th class="th_class">NºConvenio</th>
    <th class="th_class">Caducidad</th>
    <th class="th_class">Representante</th>
    <th class="th_class">CIF Empresa</th>
    <th class="th_class">Empresa</th>
    <th class="th_class">Fecha</th>
    <th class="th_class">Acciones<a href="<?= site_url('FormConvenioController/')?>"class="btn btn-warning btn-sm">Añadir</a>
</thead>
<tbody>
    <?php $titulo ?>
    <?php foreach ($convenios as $convenio): ?>
    <tr>
        <td class="bottom"><?= $convenio['nºconvenio'] ?></td>
        <td class="bottom"><?= $convenio['caducidad'] ?></td>
        <td class="bottom"><?= $convenio['representante'] ?></td>
        <td class="bottom"><?= $convenio['cif_emp'] ?></td>
        <td class="bottom"><?= $convenio['empresa'] ?></td>
        <td class="bottom"><?= $convenio['fecha'] ?></td> 
         <td class="bottom"><a href="<?= site_url('TablaConvenioController/eliminar/'.$convenio['nºconvenio'])?>" 
                              class="btn btn-danger btn-sm" onclick="return confirm('Estás seguro de borrar al alumno <?=$convenio['nºconvenio'] ?>')">Borrar</a>

                  
                  
                   
                  
                  <a href="<?= site_url('TablaConvenioController/actualiza/'.$convenio['nºconvenio'])?>"class="btn btn-primary btn-sm">Editar</a>
            
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
       
</div>
</body>
</html>
<?= $this->endSection() ?>