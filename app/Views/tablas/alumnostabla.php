<?= $this->extend('layout/layout') ?>
<?= $this->section('content') ?>
<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <title>LISTADO DE ALUMNOS</title>
    </head>

<style>

    body {
        
        background-color:#E6E6FA;
    }
    
    th {
        
        background-color:white;
    }
    
    tr {
        
        background-color:white;
    }
    
</style>
<body>
<h2 style="text-align: center;">LISTADO DE ALUMNOS</h2>
<div class="container">
<table class="table table-hover table-bordered">
<thead>
    <th class="th_class">DNI</th>
    <th class="th_class">E-Mail</th>
    <th class="th_class">Nombre</th>
    <th class="th_class">Apellido 1</th>
    <th class="th_class">Apellido 2</th>
    <th class="th_class">Teléfono</th>
    <th class="th_class">Acciones<a href="<?= site_url('FormAlumnosController')?>"class="btn btn-warning btn-sm">Añadir</a>
        
</th>
    
</thead>
<tbody>
    <?php $titulo ?>
    <?php foreach ($alumnos as $alumno): ?>
    <tr>
        <td class="bottom"><?= $alumno['DNI_ALU'] ?></td>
        <td class="bottom"><?= $alumno['EMAIL'] ?></td>
        <td class="bottom"><?= $alumno['NOMBRE'] ?></td>
        <td class="bottom"><?= $alumno['APELLIDO1'] ?></td>
        <td class="bottom"><?= $alumno['APELLIDO2'] ?></td>       
        <td class="bottom"><?= $alumno['TLF'] ?></td> 
        <td class="bottom"><a href="<?= site_url('TablaAlumnosController/eliminar/'.$alumno['DNI_ALU'])?>" 
                              class="btn btn-danger btn-sm" onclick="return confirm('Estás seguro de borrar al alumno <?=$alumno['DNI_ALU'] ?>')">Borrar</a>

                  
                  
                   
                  
                  <a href="<?= site_url('TablaAlumnosController/actualiza/'.$alumno['DNI_ALU'])?>"class="btn btn-primary btn-sm">Editar</a>
</tr>
    <?php endforeach; ?>
</tbody>
</table>
       
</div>
</body>
</html>

<?= $this->endSection() ?>