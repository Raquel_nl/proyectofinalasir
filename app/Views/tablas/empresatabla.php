<?= $this->extend('layout/layout') ?>
<?= $this->section('content') ?>
<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <title>LISTADO DE EMPRESAS</title>
    </head>

<style>

    body {
        background-color: #E6E6FA;
    }
    
    table{
        border-radius: 5px;
    }
    
    th {
        background-color:white;
    }
    
    tr {
        background-color:white;
    }

    .container, .container-lg, .container-md, .container-sm, .container-xl, .container-xxl {
        max-width: 100%;
        margin: 0px 5% 0px 4%;
    }
    
</style>
<body>
<h2 style="text-align: center;">LISTADO DE EMPRESAS</h2>
<div class="container">
<table class="table table-hover table-bordered" style="width:500px">
<thead>
    <th class="th_class">CIF Empresa</th>
    <th class="th_class">Nombre</th>
    <th class="th_class">Provincia</th>
    <th class="th_class">País</th>
    <th class="th_class">Dirección</th>
    <th class="th_class">Población</th>
    <th class="th_class">Teléfono</th>
    <th class="th_class">Código Postal</th>
    <th class="th_class">E-Mail</th>
    <th class="th_class">E-Mail PD</th>
    <th class="th_class">Representante</th>
    <th class="th_class">NIF Representante</th>
    <th class="th_class">Acciones<a href="<?= site_url('FormEmpresaController/')?>"class="btn btn-warning btn-sm">Añadir</a>
</thead>
<tbody>
    <?php $titulo ?>
    <?php foreach ($empresas as $empresa): ?>
    <tr>
        <td class="bottom"><?= $empresa->CIF ?></td>
        <td class="bottom"><?= $empresa->NOMBRE ?></td>
        <td class="bottom"><?= $empresa->PROVINCIA ?></td>
        <td class="bottom"><?= $empresa->PAIS ?></td>
        <td class="bottom"><?= $empresa->DIRECCION ?></td>
        <td class="bottom"><?= $empresa->POBLACION ?></td>
        <td class="bottom"><?= $empresa->TLF ?></td>
        <td class="bottom"><?= $empresa->CP ?></td>
        <td class="bottom"><?= $empresa->EMAIL ?></td>
        <td class="bottom"><?= $empresa->PDEMAIL ?></td>
        <td class="bottom"><?= $empresa->REPRESENTANTE ?></td>
        <td class="bottom"><?= $empresa->NIF_REPRESENTANTE ?></td>
        <td class="bottom"><a href="<?= site_url('TablaEmpresaController/eliminar/'.$empresa->CIF)?>" 
                              class="btn btn-danger btn-sm" onclick="return confirm('Estás seguro de borrar la empresa <?=$empresa->CIF ?>')">Borrar</a>

                  
                  
                   
                  
                  <a href="<?= site_url('TablaEmpresaController/actualiza/'.$empresa->CIF)?>"class="btn btn-primary btn-sm">Editar</a>
            
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
       
</div>
</body>
</html>
<?= $this->endSection() ?>