<?= $this->extend('layout/layout') ?>
<?= $this->section('content') ?>
<html lang="es">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <title>LISTADO DE CENTROS DE TRABAJO</title>
    </head>

<style>

    body {
        
        background-color:#E6E6FA;
    }
    
    th {
        
        background-color:white;
    }
    
    tr {
        
        background-color:white;
    }
    
</style>
<body>
<h2 style="text-align: center;">LISTADO DE CENTROS DE TRABAJO</h2>
<div class="container">
<table class="table table-hover table-bordered">
<thead>
    <th class="th_class">ID Centro Trabajo</th>
    <th class="th_class">Nombre</th>
    <th class="th_class">Provincia</th>
    <th class="th_class">Dirección</th>
    <th class="th_class">Teléfono</th>
    <th class="th_class">CIF Empresa</th>
    <th class="th_class">Acciones<a href="<?= site_url('TablaCTController/actualiza')?>"class="btn btn-warning btn-sm">Añadir</a>
      
</thead>
<tbody>
    <?php $titulo ?>
    <?php foreach ($centrotrabajos as $centrotrabajo): ?>
    <tr>
        <td class="bottom"><?= $centrotrabajo['ID_CT'] ?></td>
        <td class="bottom"><?= $centrotrabajo['NOMBRE'] ?></td>
        <td class="bottom"><?= $centrotrabajo['PROVINCIA'] ?></td>
        <td class="bottom"><?= $centrotrabajo['DIRECCION'] ?></td>
        <td class="bottom"><?= $centrotrabajo['TLF'] ?></td>
        <td class="bottom"><?= $centrotrabajo['CIF_EMP'] ?></td>
        <td class="bottom"><a href="<?= site_url('TablaCTController/eliminar/'.$centrotrabajo['ID_CT'])?>" 
                              class="btn btn-danger btn-sm" onclick="return confirm('Estás seguro de borrar el centro de trabajo <?=$centrotrabajo['ID_CT'] ?>')">Borrar</a>

                  
                  
                   
                  
                  <a href="<?= site_url('TablaCTController/actualiza/'.$centrotrabajo['ID_CT'])?>"class="btn btn-primary btn-sm">Editar</a>
               
    </tr>
    <?php endforeach; ?>
</tbody>
</table>
       
</div>
</body>
</html>

<?= $this->endSection() ?>