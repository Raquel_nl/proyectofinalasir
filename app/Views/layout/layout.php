<!doctype html>
<html>
    <head>
            <title>INICIO</title>
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
    </head>
    <body>
        <?= $this->include('layout/plantilla') ?>
        <div class="container"> 
        <?= $this->renderSection('content') ?>  
        <?= $this->renderSection('javascript') ?>    
    </body>
</html>