<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css"/> 
<link rel="stylesheet" href="<?= base_url('/layout/plantilla/plantilla.css')?>"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css"/>
<script  src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
<!--<script type="text/javascript" src="<?= base_url('/layout/plantilla/plantilla.js')?>"></script>-->
    


<nav class="navbar navbar-expand-custom navbar-mainbg">
        <a class="navbar-brand navbar-logo" href="InicioController">INICIO</a>
        <button class="navbar-toggler" type="button" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <i class="fas fa-bars text-white"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto">
                <div class="hori-selector"><div class="left"></div><div class="right"></div></div>
                <li class="nav-items">
                    <a class="nav-link" href="<?=site_url('TablaAlumnosController')?>">ALUMNOS</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="<?=site_url('TablaEmpresaController')?>">EMPRESAS</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" href="<?=site_url('TablaCTController')?>">CENTROS DE TRABAJO</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=site_url('TablaFPDualController')?>">FP DUALES</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=site_url('TablaConvenioController')?>">CONVENIOS</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="<?=site_url('TablaInstructorController')?>">INSTRUCTORES</a>
                </li>
            </ul>
        </div>
    </nav>